class Student
  def initialize(first_name, last_name)
    @first_name = first_name
    @last_name = last_name
    @courses = []
  end

  def first_name
    @first_name
  end

  def last_name
    @last_name
  end

  def courses
    @courses
  end

  def name
    "#{@first_name} #{@last_name}"
  end

  def enroll new_course
    if @courses.include?(new_course)
      return "#{self.name} is already enrolled"
    elsif @courses.any? { |course| course.conflicts_with?(new_course)}
      raise "#{course} conflicts with #{new_course}"
    else
    @courses << new_course
    new_course.students.push(self)
    end
  end

  def course_load
    course_load = Hash.new(0)
    @courses.each do |course|
      course_load[course.department] += course.credits
    end
    course_load
  end
end
